#!/usr/bin/env python

from __future__ import print_function
import argparse
import overpass
import sys

def main():
    parser = argparse.ArgumentParser(description='Provides Quality Assurance methods for meteorological monitoring stations in OpenStreetMap.')
    parser.add_argument('-b', '--bbox', dest='bbox',
                        metavar='bounding-box',
                        help='Specify bounding box filter, a comma separated list of 4 coordinates (lower lat, lower lon, upper lat, upper lon).',
                        required=False,
                        default=None)
    parser.add_argument('-c', '--countries', dest='countries',
                        metavar='countries',
                        help='Comma separated list of country codes (ISO 3166-1 alpha-2) to filter for.',
                        required=False,
                        default=None)
    parser.add_argument('-t', '--timeout', dest='timeout',
                        metavar="timeout",
                        type=int,
                        required=False,
                        default=25,
                        help='specify Overpass API timeout value (for bigger queries).')
    args = parser.parse_args()

    if args.bbox != None and args.countries != None:
        print('Please specify either bbox or countries filter. Exiting.', file=sys.stderr)
        sys.exit()

    if args.bbox != None:
        args.bbox = args.bbox.replace(' ', '')
        if args.bbox.count(',') != 3:
            print('Your bounding box filter seems to be invalid, please double check it. Exiting.', file=sys.stderr)
            sys.exit()
        try:
            coords = args.bbox.split(',')
            float(coords[0])
            float(coords[1])
            float(coords[2])
            float(coords[3])
        except ValueError:
            print('Your bounding box filter seems to be invalid, please double check it. Exiting.', file=sys.stderr)
            sys.exit()

    api = overpass.API(timeout=args.timeout)
    # checks for stations that have ref:wmo/ref:icao but no ele
    query = ''
    if args.countries != None:
        print('Filtering for countries: ' + args.countries, file=sys.stderr)
        args.countries = args.countries.replace(' ', '')
        args.countries = args.countries.replace(',', '|')
        query += 'area["type"="boundary"]["admin_level"="2"]["ISO3166-1:alpha2"'
        if args.countries.count('|') > 0:
            query += '~'
        else:
            query += '='
        query += '"' + args.countries + '"]->.searchArea;'
    query += '(node["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"]["ele"!~"."]'
    if args.countries != None:
        query += '(area.searchArea)'
    if args.bbox != None:
        query += '(' + args.bbox + ')'
    query += ';'
    query += 'way["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"]["ele"!~"."]'
    if args.countries != None:
        query += '(area.searchArea)'
    if args.bbox != None:
        query += '(' + args.bbox + ')'
    query += ';'
    # this covers stations which do only have ref:icao set
    query += 'node["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"!~"."]["ref:icao"]["ele"!~"."]'
    if args.countries != None:
        query += '(area.searchArea)'
    if args.bbox != None:
        query += '(' + args.bbox + ')'
    query += ';'
    query += 'way["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"!~"."]["ref:icao"]["ele"!~"."]'
    if args.countries != None:
        query += '(area.searchArea)'
    if args.bbox != None:
        query += '(' + args.bbox + ')'
    query += ';'
    query += ');'

    try:
        print('Checking for stations without elevation...', file=sys.stderr)
        response = api.Get(query)
    except Exception,e:
        print('There was an error while querying Overpass API: ' + type(e).__name__ + '. Exiting.', file=sys.stderr)
        sys.exit()

    print()
    if 'features' in response and len(response['features']) > 0:
        print('The following WMO stations have no elevation set (that means that e.g. pressure reduction will not be possible):')
        for feature in response['features']:
            output = feature['properties']['ref:wmo'] + ' ' + feature['properties']['name'] + ' '
            if 'int_name' in feature['properties']:
                output += '(' + feature['properties']['int_name'] + ') '
            output += 'https://openstreetmap.org/'
            if feature['geometry']['type'] == 'Point':
                output += 'node/'
            elif feature['geometry']['type'] == 'LineString':
                output += 'way/'
            output += str(feature['id'])
            print(output)
    else:
        print('All WMO stations currently have an elevation set. Great!')

    # checks for stations that have ref:wmo/ref:icao but are relations

    print()
    query = ''
    if args.countries != None:
        print('Filtering for countries: ' + args.countries, file=sys.stderr)
        args.countries = args.countries.replace(' ', '')
        args.countries = args.countries.replace(',', '|')
        query += 'area["type"="boundary"]["admin_level"="2"]["ISO3166-1:alpha2"'
        if args.countries.count('|') > 0:
            query += '~'
        else:
            query += '='
        query += '"' + args.countries + '"]->.searchArea;'
    query += '(relation["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"]'
    if args.countries != None:
        query += '(area.searchArea)'
    if args.bbox != None:
        query += '(' + args.bbox + ')'
    query += ';'
    # this covers stations which do only have ref:icao set
    query += 'relation["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"!~"."]["ref:icao"]'
    if args.countries != None:
        query += '(area.searchArea)'
    if args.bbox != None:
        query += '(' + args.bbox + ')'
    query += ';'
    query += ');'

    try:
        print('Checking for stations that are relations...', file=sys.stderr)
        response = api.Get(query)
    except Exception,e:
        print('There was an error while querying Overpass API: ' + type(e).__name__ + '. Exiting.', file=sys.stderr)
        sys.exit()

    if 'features' in response and len(response['features']) > 0:
        print('The following WMO stations are relations, which probably should be changed:')
        for feature in response['features']:
            output = feature['properties']['ref:wmo'] + ' ' + feature['properties']['name'] + ' '
            if 'int_name' in feature['properties']:
                output += '(' + feature['properties']['int_name'] + ') '
            output += 'https://openstreetmap.org/relation/' + str(feature['id'])
            print(output)
    else:
        print('No WMO stations currently are relations. Great!')

if __name__ == "__main__":
    main()
