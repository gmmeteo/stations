## OpenStreetMap Weather Monitoring Station inventory

[OpenStreetMap](https://openstreetmap.org) is a geographical database and as such
very well suited to store information about weather monitoring stations. It is
able to provide higher accuracy and consistency levels as [WMO Vol A](http://www.wmo.int/pages/prog/www/ois/volume-a/vola-home.htm).
See also the recent [OSCAR/Surface](https://oscar.wmo.int/surface/index.html#/) initiative by WMO.

Currently weather monitoring stations are recorded with the following tags:
````
man_made=monitoring_station
monitoring:weather=yes
ref:wmo=*
ref:icao=*
ele=*
name=*
int_name=*
````
See [this guide](https://wiki.openstreetmap.org/wiki/User:Nebulon42/Meteo) for tagging details.

The point of a station inventory is to retrieve latitude and longitude of a
given station identifier to e.g. place it on a map or interpolate it onto a
grid. The second most important value is station elevation. This is used to
compute e.g. mean sea level pressure which allows comparisons between stations.

### Output

The script produces a CSV file with the columns `wmo,icao,lat,lon,ele,name,int_name`
where `wmo` is the 5 digit WMO station ID, `icao` is the ICAO 4 letter airport code,
`lat` and `lon` are latitude and longitude respectively, `ele` is elevation in
meters above sea level, `name` is the station name which may contain non-latin
characters, and `int_name` is a latinized variant of the station name.

A warning is printed for stations where the name contains non-ASCII characters but
no `int_name` is set.

## Usage

In the simplest form you just call the script with an output file:
`./stations.py output.csv` and get a worldwide list of all stations with WMO
identifier in OpenStreetMap.

### Filters

You can filter by countries. E.g. `./stations.py -c AT,SI output.csv` will
produce a file with all stations in Austria and Slovenia.

You can filter by bounding box. E.g. `./stations.py -b 46.5343,12.7963,47.0383,13.895 output.csv` will produce a file with stations
in parts of Carinthia, Austria and Eastern Tyrol, Austria.

## Quality Assurance

The script `./qa.py` checks for stations that have no `ele` set, which would prevent
e.g. reducing pressure to mean sea level. It also checks for monitoring stations
that are relations, which could be unnecessary and is currently not supported by
the inventory extraction script. Filter parameters are the same as for `stations.py`.
