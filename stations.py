#!/usr/bin/env python

from __future__ import print_function
import argparse
import csv
import overpass
import StringIO
import sys

def main():
    parser = argparse.ArgumentParser(description='Gets WMO station inventory from OpenStreetMap.')
    parser.add_argument('output',
                        metavar='output-file',
                        help='The file where the CSV output is written to.')
    parser.add_argument('-b', '--bbox', dest='bbox',
                        metavar='bounding-box',
                        help='Specify bounding box filter, a comma separated list of 4 coordinates (lower lat, lower lon, upper lat, upper lon).',
                        required=False,
                        default=None)
    parser.add_argument('-c', '--countries', dest='countries',
                        metavar='countries',
                        help='Comma separated list of country codes (ISO 3166-1 alpha-2) to filter for.',
                        required=False,
                        default=None)
    parser.add_argument('-s', '--server', dest='server',
                        metavar='server',
                        help='Specify the Overpass API server to which the requests should be send. Defaults to http://overpass-api.de/api/interpreter',
                        required=False,
                        default='http://overpass-api.de/api/interpreter')
    parser.add_argument('-t', '--timeout', dest='timeout',
                        metavar="timeout",
                        type=int,
                        required=False,
                        default=25,
                        help='specify Overpass API timeout value (for bigger lists).')
    args = parser.parse_args()

    if args.bbox != None and args.countries != None:
        print('Please specify either bbox or countries filter. Exiting.', file=sys.stderr)
        sys.exit()

    if args.bbox != None:
        args.bbox = args.bbox.replace(' ', '')
        if args.bbox.count(',') != 3:
            print('Your bounding box filter seems to be invalid, please double check it. Exiting.', file=sys.stderr)
            sys.exit()
        try:
            coords = args.bbox.split(',')
            float(coords[0])
            float(coords[1])
            float(coords[2])
            float(coords[3])
        except ValueError:
            print('Your bounding box filter seems to be invalid, please double check it. Exiting.', file=sys.stderr)
            sys.exit()

    api = overpass.API(timeout=args.timeout, endpoint=args.server)
    # since the point of a station inventory is to have lat, lon and elevation available (e.g. for pressure reduction)
    # it does not make sense to retrieve stations that do not have ele or ref:wmo set
    query = ''
    if args.countries != None:
        print('Filtering for countries: ' + args.countries, file=sys.stderr)
        args.countries = args.countries.replace(' ', '')
        args.countries = args.countries.replace(',', '|')
        query += 'area["type"="boundary"]["admin_level"="2"]["ISO3166-1:alpha2"'
        if args.countries.count('|') > 0:
            query += '~'
        else:
            query += '='
        query += '"' + args.countries + '"]->.searchArea;'
    query += '(' + addClause('["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"]["ele"]', args)
    # this covers stations which do only have ref:icao set
    query += addClause('["man_made"="monitoring_station"]["monitoring:weather"="yes"]["ref:wmo"!~"."]["ref:icao"]["ele"]', args)
    query += ');'

    stations = []
    try:
        print('Fetching data from OpenStreetMap...', file=sys.stderr)
        stations = api.Get(query, responseformat='json', verbosity='center')
    except Exception,e:
        print('There was an error while querying Overpass API: ' + type(e).__name__ + '. Exiting.', file=sys.stderr)
        sys.exit()

    try:
        outputFile = open(args.output, 'w')
        writer = csv.DictWriter(outputFile, fieldnames=['wmo', 'icao', 'lat', 'lon', 'ele', 'name', 'int_name'],
            quoting=csv.QUOTE_ALL, delimiter=',')

        print('Writing data to file ' + args.output + '.', file=sys.stderr)
        writer.writeheader()
        writtenStations = []
        for station in stations['elements']:
            if 'lat' in station and 'lon' in station:
                item = {}

                if 'ref:wmo' in station['tags']:
                    if station['tags']['ref:wmo'] not in writtenStations:
                        writtenStations.append(station['tags']['ref:wmo'])
                    else:
                        continue
                elif 'ref:icao' in station['tags']:
                    if station['tags']['ref:icao'] not in writtenStations:
                        writtenStations.append(station['tags']['ref:icao'])
                    else:
                        continue
                # do not write out stations without id
                else:
                    continue

                if 'ref:wmo' in station['tags']:
                    item['wmo'] = station['tags']['ref:wmo']
                else:
                    item['wmo'] = ''
                if 'ref:icao' in station['tags']:
                    item['icao'] = station['tags']['ref:icao']
                else:
                    item['icao'] = ''
                if 'name' in station['tags']:
                    item['name'] = station['tags']['name']
                else:
                    item['name'] = ''
                if 'int_name' in station['tags']:
                    item['int_name'] = station['tags']['int_name']
                else:
                    item['int_name'] = ''
                if 'ele' in station['tags']:
                    item['ele'] = station['tags']['ele']
                else:
                    item['ele'] = ''
                item['lat'] = '{:.7f}'.format(station['lat'])
                item['lon'] = '{:.7f}'.format(station['lon'])

                writer.writerow({k:v.encode('utf8') for k,v in item.items()})

                try:
                    station['tags']['name'].decode('ascii')
                except UnicodeEncodeError:
                    if item['int_name'] == '':
                        print('Warning: the name of station ' + item['wmo'] + ' contains non-ascii characters but no int_name is set.')
    except IOError:
        sys.exit('Could not open output file. Exiting.')

def addClause(clause, args):
    stmt = 'node' + clause
    if args.countries != None:
        stmt += '(area.searchArea)'
    if args.bbox != None:
        stmt += '(' + args.bbox + ')'
    stmt += ';'

    stmt += 'way' + clause
    if args.countries != None:
        stmt += '(area.searchArea)'
    if args.bbox != None:
        stmt += '(' + args.bbox + ')'
    stmt += ';'

    return stmt

if __name__ == "__main__":
    main()
